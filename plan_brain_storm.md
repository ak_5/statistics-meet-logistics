 
| Step | comments |
| ------ | ------ |
| Dataset Cleaning | NAN Werte, komische Formaten |
| Feature Engineering | Aggregation für die throughput über measurement, Mobilfunknetzbetreiber und pci. Neue Merkmale durch die Wettervorhersage schaffen, rssi und ASU Berechnen, |
| Data Exploration | Destribution und Schiefe der nummerischen Merkmale , Box plot und Ausreißer, Korrelation mit Heatmap, |
| Feature Encoding | OneHotEncoder(pci) |
| Feature Selection | feature importance (RF & XGB)|
| model Training | XGBoost Regressor, RFRegressor, SVM with RBF, Regression Tree |
| model Selection | Beste Modellen bestimmen |
| Hyperparameter Optimization | Random Search, Grid Search |
| Results | Visulization: RMSE, R2, learning Rate  |


Bibliotheken: 
Pandas, Numpy, XGBoost, SKlearn, Pytoruch, seaborn , Matplotlib, Stopwatch.py, 

 

    
